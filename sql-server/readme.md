# SQL Server in a Container

You should be able to login to the sql prompt from the terminal

```bash
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P yourStrongPassword123
```

Also you should be able to connect using SSMS to your `localhost:1433`